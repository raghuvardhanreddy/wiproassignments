#!/bin/bash

DEBUG_MODE=1

debug_msg() {

  if [ "$DEBUG_MODE" -eq 1 ]; then

    echo "[DEBUG] $1"

  fi

}

if [ -d "TestDir" ]; then

  echo "Error: Directory 'TestDir' already exists."

  exit 1

fi

if ! mkdir "TestDir"; then

  echo "Error: Unable to create directory 'TestDir'. Check permissions."

  exit 1

fi

debug_msg "Created directory 'TestDir'"


cd "TestDir" || exit

for ((i=1; i<=10; i++)); do

  filename="File${i}.txt"

  if ! echo "$filename" > "$filename"; then

    echo "Error: Unable to create file '$filename'. Check permissions."

    exit 1

  fi

  debug_msg "Created file '$filename'"

done

echo "Files created successfully in TestDir."

