#!/bin/bash

mkdir -p TestDir

cd TestDir || exit

for ((i=1; i <= 10; i++)); do
     filename="File${i}.txt"
     echo "$filename" > "$filename"
done

echo "Files created successfully in  TestDir"
