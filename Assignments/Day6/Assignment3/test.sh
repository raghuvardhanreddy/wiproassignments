#!/bin/bash

count_lines() {

filename="$1"
if [ -f "$filename" ];then
    lines=$(wc -l < "$filename")
    echo "Number of lines in $filename: $lines"
else
    echo  "File $filename not found"
fi
}


count_lines "sample.txt"
count_lines "sample2.txt"
