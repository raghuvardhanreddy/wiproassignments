#!/bin/bash

while true; do
    read -p "Enter a number (Enter  0 to exit): " number
    if [ "$number" -eq 0 ]; then
       echo "exiting"
       break
    elif [ $((number % 2)) -eq 0 ]; then
        echo "$number is even"
    else
        echo "$number is odd"
   fi
done
