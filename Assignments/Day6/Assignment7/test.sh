#!/bin/bash

if [ "$#" -ne 3 ]; then
   echo "Usage: input_file old_text new_text" 
   exit 1
fi


input_file="$1"
old_text="$2"
new_text="$3"
output_file="${input_file%.txt}_updated.txt"

sed "s/$old_text/$new_text/g" "$input_file" > "$output_file"

echo "Replacement complete. Output saved to $output_file"

