#!/bin/bash

logfile="logfile.log"

grep "ERROR" "$logfile" | awk -F'[][]' '{print $2, $3, $5}'
