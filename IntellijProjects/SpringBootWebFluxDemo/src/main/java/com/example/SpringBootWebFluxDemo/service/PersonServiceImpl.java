package com.example.SpringBootWebFluxDemo.service;

import com.example.SpringBootWebFluxDemo.model.Person;
import com.example.SpringBootWebFluxDemo.repository.PersonRepository;
import org.springframework.stereotype.Service;


import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class PersonServiceImpl implements PersonServiceInterface{

    @Autowired
    PersonRepository personRepository;

   @Override
    public void createPerson(Person person) {
        personRepository.save(person).subscribe();
    }

    @Override
    public Flux<Person> findAllPerson() {
        return personRepository.findAll();
    }

    @Override
    public Mono<Person> findByPersonId(Integer id) {
        return personRepository.findById(id);
    }
}
