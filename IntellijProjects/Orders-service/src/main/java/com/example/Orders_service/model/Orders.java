package com.example.Orders_service.model;

import jakarta.persistence.*;
import org.hibernate.query.Order;

@Entity
@Table(name = "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int order_id;
    private int product_id;
    private int order_number;
    private  String order_status;


    public Orders(){

    }

    public Orders(int product_id, int order_number, String order_status) {
        this.product_id = product_id;
        this.order_number = order_number;
        this.order_status = order_status;

    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getOrder_number() {
        return order_number;
    }

    public void setOrder_number(int order_number) {
        this.order_number = order_number;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "order_id=" + order_id +
                ", product_id=" + product_id +
                ", order_number=" + order_number +
                ", order_status='" + order_status + '\'' +
                '}';
    }
}
