package com.example.order_service.model;


import jakarta.persistence.*;

@Entity
@Table(name="order")

public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int order_id;
    private int product_id;
    private double order_number;
    private String order_status;

    public Order(){

    }

    public Order(int product_id, double order_number, String order_status) {

        this.product_id = product_id;
        this.order_number = order_number;
        this.order_status = order_status;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public double getOrder_number() {
        return order_number;
    }

    public void setOrder_number(double order_number) {
        this.order_number = order_number;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    @Override
    public String toString() {
        return "Order{" +
                "order_id=" + order_id +
                ", product_id=" + product_id +
                ", order_number=" + order_number +
                ", order_status='" + order_status + '\'' +
                '}';
    }
}
