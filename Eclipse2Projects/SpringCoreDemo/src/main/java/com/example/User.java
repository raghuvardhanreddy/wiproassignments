package com.example;

public class User {
	private int id;
	private String email;
	private String password;
	private Skills skill;
	
	User(){
		System.out.println("Default constructor Invoked...");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	//parameterized constructor

	public User(int id, String email, String password, Skills skill) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.skill = skill;
	}

	void getUserInfo() {
		System.out.println(id+" "+email+" "+password);
		System.out.println(skill.toString());
	}
	
	


	
	
	

}
