package com.mystore;

public class Products {
	
	private int product_id;
	private String product_name;
	private double product_price;
	private Orders order;
	
	Products(){
		System.out.println("Default constructor Invoked...");
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public double getProduct_price() {
		return product_price;
	}

	public void setProduct_price(double product_price) {
		this.product_price = product_price;
	}

	public Products(int product_id, String product_name, double product_price, Orders order) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_price = product_price;
		this.order = order;
	}
	
	void getProductInfo() {
		System.out.println(product_id+" "+product_name+" "+product_price);
		System.out.println(order.toString());
	}
	

}
