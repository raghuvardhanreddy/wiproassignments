package demo;

import java.io.Serializable;

public class Product implements Serializable{
	
	private int p_id;
	private String p_name;
	private int p_qty;
	private double p_price;
	private String p_mfg;
	private String p_expiry;
	public int getP_id() {
		return p_id;
	}
	public void setP_id(int p_id) {
		this.p_id = p_id;
	}
	public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	
	public int getP_qty() {
		return p_qty;
	}
	public void setP_qty(int p_qty) {
		this.p_qty = p_qty;
	}
	public double getP_price() {
		return p_price;
	}
	public void setP_price(double p_price) {
		this.p_price = p_price;
	}
	public String getP_mfg() {
		return p_mfg;
	}
	public void setP_mfg(String p_mfg) {
		this.p_mfg = p_mfg;
	}
	public String getP_expiry() {
		return p_expiry;
	}
	public void setP_expiry(String p_expiry) {
		this.p_expiry = p_expiry;
	}
	
	
	
	
	
	

}
