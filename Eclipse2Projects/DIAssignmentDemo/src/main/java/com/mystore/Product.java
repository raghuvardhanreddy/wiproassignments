package com.mystore;

public class Product {
	int product_id;
	String product_name;
	double product_price;
	int product_qty;
	
	Product(){
		System.out.println("Default constructor Invoked...");
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String produc_name) {
		this.product_name = produc_name;
	}

	public double getProduct_price() {
		return product_price;
	}

	public void setProduct_price(double product_price) {
		this.product_price = product_price;
	}

	public int getProduct_qty() {
		return product_qty;
	}

	public void setProduct_qty(int product_qty) {
		this.product_qty = product_qty;
	}
	
	void getProductInfo() {
		System.out.println(this.product_id+" "+this.product_name+" "+
	      this.product_price+" "+this.product_qty);
	}
	

}
