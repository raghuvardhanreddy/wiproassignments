package com.mystore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestProduct {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	 ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
	 Product pObj = (Product) context.getBean("productBean");
	 pObj.getProductInfo();
	}

}
