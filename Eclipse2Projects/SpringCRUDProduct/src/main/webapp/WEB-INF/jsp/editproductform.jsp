<%@ page language="java" contentType="text/html; charset=windows-1256"
    pageEncoding="windows-1256"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="windows-1256">
<title>Insert title here</title>
</head>
<body>
	<h2>Update User Form</h2>
	<form:form method="POST" action="/SpringMVCRUDemo/updateproduct">
		<table>
			<tr>
			<td>Product Id</td>
			<td><form:input path="product_id" /></td>
			</tr>
			<tr>
			<td>Product Name</td>
			<td><form:input path="product_name" /></td>
			</tr>
			<tr>
			<td>Product Price</td>
			<td><form:input path="product_price" /></td>
			</tr>
			<td>Product Quantity</td>
			<td><form:input path="product_qty" /></td>
			</tr>
			<tr>
			<td><input type="submit" value="Update Product" /></td>
			</tr>
			
		</table>
	</form:form>
	
</body>
</html>