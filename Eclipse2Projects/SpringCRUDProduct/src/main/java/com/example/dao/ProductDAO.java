package com.example.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.example.beans.Product;

public class ProductDAO {
	
	JdbcTemplate template;
	
	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}
	
	public int saveProduct(Product product) {
		String query="insert into product(product_id,product_name,product_price,product_qty) values('"+product.getProduct_id()+"','"+product.getProduct_name()+"','"+product.getProduct_price()+"','"+product.getProduct_qty()+"')";
		return template.update(query);
	}
	
	public List<Product> getProduct(){
		return template.query("select * from product",new RowMapper<Product>() {
			@Override
			public Product mapRow(ResultSet rs,int rowNo) throws SQLException {
				Product p = new Product();
				p.setProduct_id(rs.getInt(1));
				p.setProduct_name(rs.getString(2));
				p.setProduct_price(rs.getDouble(3));
				p.setProduct_qty(rs.getInt(4));
				return p;
			}
			
			
		});
		
	}
	
	public Product getProductById(int id) {
		String query="select * from product where product_id=?";
		return template.queryForObject(query, new Object[]{id},new BeanPropertyRowMapper<Product>(Product.class));
		
	}
	
	public int update(Product product){
		String query = "update product set product_name='"+product.getProduct_name()+"',product_price='"+product.getProduct_price()+"',product_qty='"+product.getProduct_qty()+"' where id="+product.getProduct_id()+"";
		return template.update(query);
	}
	
	public int delete(int product_id) {
		String query="delete * from Product where product_id="+product_id+"";
		return template.update(query);
	}

}
