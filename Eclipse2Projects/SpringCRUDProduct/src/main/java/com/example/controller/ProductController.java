package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.beans.Product;
import com.example.dao.ProductDAO;

@Controller
public class ProductController {
	
	@Autowired
	ProductDAO dao;
	
	@RequestMapping("/productsform")
	public String displayProductForm(Model model){
		model.addAttribute("command",new Product());
		return "productsform";
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String saveProduct(@ModelAttribute("product")Product product){
		dao.saveProduct(product);
		return "redirect:/listproducts";
	}
	
	
	@RequestMapping("/listproducts")
	public String listProducts(Model model){
		List<Product> list= dao.getProduct();
		model.addAttribute("list",list);
		return "listproducts";
	}
	
	@RequestMapping(value="/listproducts/{id}")
	public String getProductById(@PathVariable int id, Model model){
		Product product = dao.getProductById(id);
		model.addAttribute("command",product);
		return "editproductform";
	}
	
	@RequestMapping(value="/updateproduct",method=RequestMethod.POST)
	public String updateProduct(@ModelAttribute("product")Product product){
		dao.update(product);
		return "redirect:/listproducts";
	}
	
	@RequestMapping(value="/deleteproduct/{id}")
	public String deleteProduct(@PathVariable int id){
		dao.delete(id);
		return "redirect:/listproducts";
	}
	

}
