package com.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
   ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
   City cObj=(City) ctx.getBean("cityBean",City.class);
   cObj.setId(101);
   cObj.setCity_name("BLR");
   State sObj = ctx.getBean("stateBean",State.class);
   cObj.setState(sObj);
   cObj.getCityDetails();

   
	}

}
