package com.example;

import org.springframework.beans.factory.annotation.Autowired;

public class City {
	private int id;
	private String city_name;
	private State state;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public State getState() {
		return state;
	}
	
	@Autowired
	public void setState(State state) {
		this.state = state;
	}
	
	
	public void getCityDetails() {
		System.out.println("City ID: "+id);
		System.out.println("City Name: "+city_name);
		System.out.println("State: "+this.state.getState_name());
	}
	

}
