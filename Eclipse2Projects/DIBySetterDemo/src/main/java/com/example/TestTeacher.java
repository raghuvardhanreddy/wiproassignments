package com.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestTeacher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Teacher tObj = (Teacher) context.getBean("teacherBean");
		Teacher tObj2 = (Teacher) context.getBean("teachersBean");

		tObj.getTeacherInfo();
		tObj2.getTeacherInfo();

	}

}
