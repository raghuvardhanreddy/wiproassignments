package javathreads;


class Client5{
	int amount=20000;
	synchronized void withdraw(int amount) {
		System.out.println("amount withdrawal...");
		if(this.amount<amount) {
			System.out.println("Insuffiecient funds");
		
		try {
			wait();
		}catch(Exception ex) {
			System.out.println(ex);
		}
		this.amount=this.amount-amount;
		}
		System.out.println("fund transfer completed !!");

	}
	
	synchronized void deposit(int amount) {
		System.out.println("amount to be credited...");
		this.amount=this.amount+amount;
		System.out.println("Funds credited successfully");
		notify();
		
	}
}
public class Test6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Client5 obj=new Client5();
		
		new Thread() {
			public void run() {
				obj.withdraw(25000);
			}
			
		}.start();
		
		new Thread() {
			public void run() {
				obj.deposit(10000);
			}
			
		}.start();

	}

}
