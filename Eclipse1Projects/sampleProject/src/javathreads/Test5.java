package javathreads;


class Task3{
	//syncronized method:used to lock an object for shared resources
	synchronized static void printing(int page) {
		for(int i=1;i<=5;i++) {
		System.out.println("Printing page "+ i+": "+ page);
		
		try {
		Thread.sleep(500);
		}catch (Exception e) {
			System.out.println(e);
		}
	}
	}
	
}


class C1 extends Thread{
	public void run() {
		Task3.printing(1);
	}
}

class C2 extends Thread{
	public void run() {
		Task3.printing(10);
	}
}

class C3 extends Thread{
	public void run() {
		Task3.printing(100);
	}
}

class C4 extends Thread{
	public void run() {
		Task3.printing(1000);
	}
}

public class Test5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		C1 obj1=new C1();
		C2 obj2=new C2();
		C3 obj3=new C3();
		C4 obj4=new C4();
		
		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();

	}

}
