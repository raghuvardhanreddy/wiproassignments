package assignments;

import java.util.Scanner;

public class ProducerConsumer {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		final Pc pc =new Pc();
		Thread t1 = new Thread(new Runnable() 
		{
			@Override
			public void run()
			{
				try
				{
					pc.produce();
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();			
					}
			}
		});
		Thread t2= new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					pc.consume();
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
	}
	
	public static class  Pc{
		public  void produce() throws InterruptedException {
			synchronized(this) {
				System.out.println("producer thread running");
				wait();
				System.out.println("Resumed");
			}
		}
		
		public void consume() throws InterruptedException {
			Thread.sleep(1000);
			Scanner s=new Scanner(System.in);
			
			synchronized(this) {
				System.out.println("waiting for return key");
				s.nextLine();
				System.out.println("Return key pressed");
				
				notify();
				
				Thread.sleep(2000);
				}
		}
		
	}

}
