package assignments;
import java.time.ZonedDateTime;

import java.time.LocalDateTime;

import java.time.format.DateTimeFormatter;

import java.time.ZoneId;

import java.util.Scanner;

public class Timestamp{

public static void main(String[] args) {

	Scanner scanner = new Scanner(System.in);

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	try {

		// Prompt the user for the input time

		System.out.print("Enter the date and time (yyyy-MM-dd HH:mm): ");

		String dateTimeInput = scanner.nextLine();

		LocalDateTime localDateTime = LocalDateTime.parse(dateTimeInput, formatter);

		// Prompt the user for the source timezone

		System.out.print("Enter the source timezone (e.g., America/New_York): ");

		String sourceTimeZone = scanner.nextLine();

		ZoneId sourceZoneId = ZoneId.of(sourceTimeZone);

		// Prompt the user for the target timezone

		System.out.print("Enter the target timezone (e.g., Europe/London): ");

		String targetTimeZone = scanner.nextLine();

		ZoneId targetZoneId = ZoneId.of(targetTimeZone);

		// Create a ZonedDateTime object for the source timezone

		ZonedDateTime sourceZonedDateTime = ZonedDateTime.of(localDateTime, sourceZoneId);

		// Convert the time to the target timezone

		ZonedDateTime targetZonedDateTime = sourceZonedDateTime.withZoneSameInstant(targetZoneId);

		// Display the converted time

		System.out.println("Converted time: " + targetZonedDateTime.format(formatter) + " " + targetZoneId);

	} catch (Exception e) {

		System.out.println("Error: " + e.getMessage());

		e.printStackTrace();

	} finally {

		scanner.close();

	}

}

}
