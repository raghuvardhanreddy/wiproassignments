package assignments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WordFrequencyCounter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String inputFilePath = "input.txt";

		String outputFilePath = "output.txt";

		// Create a map to store the word frequencies

		Map<String, Integer> wordCountMap = new HashMap<>();

		// Read the input file and count the word frequencies

		try (FileReader fr = new FileReader(inputFilePath);

				BufferedReader br = new BufferedReader(fr)) {

			String line;

			while ((line = br.readLine()) != null) {

				String[] words = line.split("\\W+");

				for (String word : words) {

					if (!word.isEmpty()) {

						word = word.toLowerCase(); // Convert to lower case

						wordCountMap.put(word, wordCountMap.getOrDefault(word, 0) + 1);

					}

				}

			}

		} catch (IOException e) {

			e.printStackTrace();

		}

		// Write the word frequencies to the output file

		try (FileWriter fw = new FileWriter(outputFilePath);

				BufferedWriter bw = new BufferedWriter(fw)) {

			for (Map.Entry<String, Integer> entry : wordCountMap.entrySet()) {

				bw.write(entry.getKey() + ": " + entry.getValue());

				bw.newLine();

			}

		} catch (IOException e) {

			e.printStackTrace();

		}










	}

}
