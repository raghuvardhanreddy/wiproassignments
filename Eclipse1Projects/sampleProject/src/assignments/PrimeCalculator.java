package assignments;

import java.io.IOException;

import java.io.PrintWriter;

import java.nio.file.Files;

import java.nio.file.Paths;

import java.util.ArrayList;

import java.util.List;

import java.util.concurrent.CompletableFuture;

import java.util.concurrent.ExecutionException;

import java.util.concurrent.ExecutorService;

import java.util.concurrent.Executors;

import java.util.concurrent.Future;

public class PrimeCalculator {

  // Function to check if a number is prime

  public static boolean isPrime(int number) {

    if (number <= 1) return false;

    for (int i = 2; i <= Math.sqrt(number); i++) {

      if (number % i == 0) return false;

    }

    return true;

  }

  // Function to calculate prime numbers up to a given number using ExecutorService

  public static List<Integer> calculatePrimes(int maxNumber, ExecutorService executor) {

    List<Future<Integer>> futures = new ArrayList<>();

    List<Integer> primes = new ArrayList<>();

    for (int i = 2; i <= maxNumber; i++) {

      final int num = i;

      futures.add(executor.submit(() -> {

        if (isPrime(num)) {

          return num;

        }

        return null;

      }));

    }

    for (Future<Integer> future : futures) {

      try {

        Integer prime = future.get();

        if (prime != null) {

          primes.add(prime);

        }

      } catch (InterruptedException | ExecutionException e) {

        e.printStackTrace();

      }

    }

    return primes;

  }

  // Function to write prime numbers to a file asynchronously using CompletableFuture

  public static CompletableFuture<Void> writePrimesToFile(List<Integer> primes, String filePath) {

    return CompletableFuture.runAsync(() -> {

      try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(Paths.get(filePath)))) {

        for (Integer prime : primes) {

          writer.println(prime);

        }

      } catch (IOException e) {

        e.printStackTrace();

      }

    });

  }

  public static void main(String[] args) {

    int maxNumber = 10000; // Example max number

    String outputFilePath = "primes.txt";

    ExecutorService executor = Executors.newFixedThreadPool(10);

    // Calculate primes in parallel

    List<Integer> primes = calculatePrimes(maxNumber, executor);

    // Shut down the executor

    executor.shutdown();

    // Write primes to a file asynchronously

    CompletableFuture<Void> future = writePrimesToFile(primes, outputFilePath);

    // Wait for the writing to complete

    future.join();

    System.out.println("Prime numbers calculation and writing to file completed.");

  }

}