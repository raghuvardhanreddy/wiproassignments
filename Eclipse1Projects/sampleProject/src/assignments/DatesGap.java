package assignments;


import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class DatesGap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy-MM-dd");
		try {

			System.out.println("Enter the first date(yyyy-MM-dd :");
			String firstDateInput=sc.next();
			LocalDate firstDate=LocalDate.parse(firstDateInput,formatter);

			System.out.print("Enter the second date(yyyy-MM-dd :");
			String secondDateInput=sc.nextLine();
			LocalDate secondDate=LocalDate.parse(secondDateInput,formatter);

			Period period = Period.between(firstDate, secondDate);

			long totalDays=ChronoUnit.DAYS.between(firstDate, secondDate);

			System.out.println("Number of days between "+firstDate +" and "+ secondDate+ ": "+totalDays);
			System.out.println("Period: "+period.getYears()+" years, "+period.getMonths()+
					" months, "+period.getDays()+" days");


		}catch(Exception e) {
			System.out.println("error parsing dates.please ensure the dates are in format yyyy-mm-dd");
		}finally {
			sc.close();
		}

	}

}
