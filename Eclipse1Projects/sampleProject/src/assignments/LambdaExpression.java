package assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Person{
	private String name;
	private int age;
	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	
	
}

public class LambdaExpression {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Person> people=new ArrayList<>();
		people.add(new Person("Alice",30));
		people.add(new Person("Bob",25));
		people.add(new Person("Charlie",35));
		
		Collections.sort(people,(p1,p2) -> Integer.compare(p1.getAge(),p2.getAge()));
		
		for(Person person:people) {
			System.out.println(person.getName()+": "+person.getAge());
		}
	}

}
