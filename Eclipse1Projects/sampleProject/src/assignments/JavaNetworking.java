package assignments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class JavaNetworking {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String urlString="https://www.javatpoint.com";
		try {
			URL url=new URL(urlString);
			HttpsURLConnection connection=(HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			int responseCode=connection.getResponseCode();
			System.out.println("Response code: "+responseCode);

			Map<String, List<String>> headers=connection.getHeaderFields();
			System.out.println("Response Headers: ");
			for(Map.Entry<String,List<String>>entry : headers.entrySet()) {
				System.out.println(entry.getKey() + ": "+entry.getValue());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			StringBuilder sb=new StringBuilder();
			while((inputLine=br.readLine()) != null) {
				sb.append(inputLine);
				sb.append("\n");

			}
			br.close();

			System.out.println("Response Body");
			System.out.println(sb.toString());

		}catch(IOException e) {
			e.printStackTrace();
		}

	}

}
