package assignments;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Counter{
	private int count=0;
	
	public synchronized void increment() {
		count++;
	}
	public synchronized void decrement() {
		count--;
		
	}
	
	public synchronized int getValue() {
		return count;
	}
	
}


final class ImmutableData{
	private final String data;
	
	public ImmutableData(String data) {
		this.data=data;
	}
	public String getData() {
		return data;
	}
}


public class ThreadSafeDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Counter counter = new Counter();
		ImmutableData immutableData =new ImmutableData("Shared Data");
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		
		Runnable incrementTask =() -> {
			for(int i =0; i< 100;i++) {
				counter.increment();
				System.out.println(Thread.currentThread().getName() + " -Incremented: "+counter.getValue());
				
			}
		};
		
		Runnable decrementTask =() -> {
			for(int i =0; i< 100;i++) {
				counter.decrement();
				System.out.println(Thread.currentThread().getName() + " -Decrement: "+counter.getValue());
				
			}
		};
		
		for(int i=0;i<3;i++) {
			executorService.submit(incrementTask);
			executorService.submit(incrementTask);
			
		}
		executorService.shutdown();
		
		
		System.out.println("Immutable Data: "+immutableData.getData());
	}

}
