package jdbcdemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Dbconnect1 {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/raghu","root","rps@");
			String query ="select * from customers where name=? and city=?";
			PreparedStatement myStmt=con.prepareStatement(query);
			myStmt.setString(1, "sachin");
			myStmt.setString(2, "mumbai");
			
			ResultSet rs= myStmt.executeQuery();
			while(rs.next()) {
				int id=rs.getInt("id");
				String name=rs.getString("name");
				String email=rs.getString("email");
				String city= rs.getString("city");
				System.out.println(id+" "+name +" "+email+ " "+city);
			}
			con.close();
			
			
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
	}
