package jdbcdemo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DBconnection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/raghu","root","rps@");
			Statement stmt=connection.createStatement();
			ResultSet rs=stmt.executeQuery("SELECT * FROM customers");
			while(rs.next()) {
				System.out.println(rs.getInt("id")+" "+rs.getString("name")+" "+rs.getString("email")+
						" "+rs.getInt("mobile")+" "+rs.getString("city"));
			}
			rs.close();
			stmt.close();
			connection.close();
			
		}catch(Exception e) {
			System.out.println(e);
			
	}

}
	}
