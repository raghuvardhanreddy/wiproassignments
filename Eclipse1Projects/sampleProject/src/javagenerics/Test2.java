package javagenerics;


public class Test2 {
	
	public <T> void genericMethod(T data) {
		System.out.println("Generic method: ");
		System.out.println("Data passed :"+ data);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test2 obj=new Test2();
		obj.<String>genericMethod("java generics");
		obj.<Integer>genericMethod(200);

	}

}
