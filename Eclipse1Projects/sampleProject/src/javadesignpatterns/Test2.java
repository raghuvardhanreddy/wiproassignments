package javadesignpatterns;

final class Person{
	
	private int id;
	private String name;
	private String address;
	
	public int getId() {
		return id;
	}
	public Person setId(int id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public Person setName(String name) {
		this.name = name;
		return this;
	}
	public String getAddress() {
		return address;
	}
	public Person setAddress(String address) {
		this.address = address;
		return this;
	}
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", address=" + address + "]";
	}
	
	
}

public class Test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person p1 = new Person();
		Person p2 = new Person();
		
		p1.setId(101).setName("Alex").setAddress("USA");
		p2.setId(102).setName("John").setAddress("UAE");
		
		System.out.println(p1);
		System.out.println(p2);

	}

}
