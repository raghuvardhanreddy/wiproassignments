package javadesignpatterns;

//Singleton design pattern
public class User {
	
	public static final User instance = new User();
	
	private User() {
		
	}
	
	public static User geInstance() {
		return instance;
	}

	

}
