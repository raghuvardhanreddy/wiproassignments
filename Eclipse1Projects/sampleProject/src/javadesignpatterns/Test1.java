package javadesignpatterns;

interface Product{
	void getProductDetail();
}

class ConcreteAProduct implements Product{
	
	@Override
	public void getProductDetail() {
		System.out.println("This is Concrete class : Concrete A Product" );
	}
}

class ConcreteBProduct implements Product{
	
	@Override
	public void getProductDetail() {
		System.out.println("This is Concrete class : Concrete B Product");
	}
}

interface Factory{
	Product factoryMethod();
}

class ConcreteFactoryA implements Factory{
	@Override
	public Product factoryMethod() {
		return new ConcreteAProduct();
	}
}

class ConcreteFactoryB implements Factory{
	
	@Override
	public Product factoryMethod() {
		return new ConcreteBProduct();
	}
	
}

public class Test1 {
	
	public static void main(String[] args) {
		Factory fA=new ConcreteFactoryA();
		Product pA=fA.factoryMethod();
		pA.getProductDetail();
		
		
		Factory fB=new ConcreteFactoryB();
		Product pB=fB.factoryMethod();
		pB.getProductDetail();
	}

}
