package junitdemo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

public class Test2 {
	private Calculations calc = new Calculations();
	
	@Test
	void testAssertEquals() {
		int expected = 50;
		int actual =calc.add(20, 50);
		assertEquals(expected,actual);
	}
	
	@Test
	void testAssertNotEquals() {
		int expected = 30;
		int actual =calc.add(20, 50);
		assertNotEquals(expected,actual);
	}
	
	@Test
	void testAsserTrue() {
		int num=0;
		assertTrue(calc.isPositive(num));
		
	}
	
	@Test
	void testAsserFalse() {
		int num= -10;
		assertFalse(calc.isPositive(num));
		
	}
	

}
