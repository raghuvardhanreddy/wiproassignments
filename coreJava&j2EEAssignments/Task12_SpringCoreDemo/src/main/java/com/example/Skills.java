package com.example;

public class Skills {
	private String  skill_name;
	private int experience;
	private String level;
	public Skills(String skill_name, int experience, String level) {
		super();
		this.skill_name = skill_name;
		this.experience = experience;
		this.level = level;
	}
	@Override
	public String toString() {
		return "Skills [skill_name=" + skill_name + ", experience=" + experience + ", level=" + level + "]";
	}
	
	
	

}
