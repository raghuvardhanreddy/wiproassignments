package assignments;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

class Persons{
	private String name;
	private int age;
	private String email;
	public Persons(String name, int age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", email=" + email + "]";
	}
	
	
	
	
}

public class PersonOperation {
	
	public static void operateOnPerson(
			Persons person,
			Predicate<Persons>predicate,
			Function<Persons,String>function,
			Consumer<Persons> consumer,
			Supplier<Persons> supplier
			) {
		
		boolean predicateResult = predicate.test(person);
		System.out.println("Predicate: "+ predicateResult);
		
		String functionResult = function.apply(person);
		System.out.println("Function result: "+functionResult);
		
		consumer.accept(person);
		System.out.println("Person after coonsumer action: "+person);
		
		Persons suppliedPerson = supplier.get();
		System.out.println("Person from Supplier: "+suppliedPerson);
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Persons person =new Persons("sachin",30,"sachin@wipro.com");
		Predicate<Persons> isAdult = p -> p.getAge() >= 18;
		Function<Persons,String> getEmail = Persons::getEmail;
		Consumer<Persons> updateEmail = p -> p.setEmail("tendulkar@wipro.com");
		Supplier<Persons> newPersonSupplier =() -> new Persons("rahul",25,"rahul@wipro.com");
		
		operateOnPerson(person,isAdult,getEmail,updateEmail,newPersonSupplier);
		
		
		

	}

}
