package com.mystore;

public class Orders {
	
	private int order_id;
	private double order_amt;
	private String order_date;
	private String order_address;
	
	


	public Orders(int order_id, double order_amt, String order_date, String order_address) {
		super();
		this.order_id = order_id;
		this.order_amt = order_amt;
		this.order_date = order_date;
		this.order_address = order_address;
	}




	@Override
	public String toString() {
		return "Orders [order_id=" + order_id + ", order_amt=" + order_amt + ", order_date=" + order_date
				+ ", order_address=" + order_address + "]";
	}
	
	
	

	



}
