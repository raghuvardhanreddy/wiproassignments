package assignments;

class Client5 {

	int amount = 20000;

	synchronized void withdraw(int amount) {

		System.out.println("Attempting to withdraw " + amount);

		while (this.amount < amount) {

			System.out.println("Insufficient funds, waiting for deposit...");

			try {

				wait();

			} catch (InterruptedException ex) {

				System.out.println(ex);

			}

		}

		this.amount -= amount;

		System.out.println("Withdrawal of " + amount + " completed. Remaining balance: " + this.amount);

	}

	synchronized void deposit(int amount) {

		System.out.println("Depositing " + amount);

		this.amount += amount;

		System.out.println("Deposit of " + amount + " completed. Current balance: " + this.amount);

		notify();

	}

}
public class Task4 {
	public static void main(String[] args) {

		Client5 account = new Client5();

		// Thread for withdrawal

		new Thread() {

			public void run() {

				account.withdraw(25000);

			}

		}.start();

		// Thread for deposit

		new Thread() {

			public void run() {

				account.deposit(10000);

			}

		}.start();

	}

}
