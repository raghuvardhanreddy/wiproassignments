package assignments;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class SampleClass{
	private String privateField="initial Value";
	public SampleClass() {
		
	}
	public void publicMethod() {
		System.out.println("Public method called.");
	}
	
	private void privateMethod() {
		System.out.println("private method called");
	}
}

public class ReflectionAPI {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		try {
			Class<?> clazz = Class.forName("assignments.SampleClass");
			
			Constructor<?>[] constructors = clazz.getDeclaredConstructors();
			System.out.println("Constructors");
			for(Constructor<?> constructor : constructors) {
				System.out.println(constructor);
			}
			
			Method[] methods = clazz.getDeclaredMethods();
			System.out.println("Methods");
			for(Method method :methods) {
				System.out.println(method);
			}
			
			Field[] fields = clazz.getDeclaredFields();
			System.out.println("Fields");
			for(Field field : fields) {
				System.out.println(fields);
			}
			
			Object instance =clazz.getDeclaredConstructor().newInstance();
			Field privateField=clazz.getDeclaredField("privateField");
			
			privateField.setAccessible(true);
			privateField.set(instance, "New Value");
			
			System.out.println("Modified privatefield value: "+privateField.get(instance));
			
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
