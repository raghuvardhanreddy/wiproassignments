package com.example.SpringBootWebFluxDemo.repository;

import com.example.SpringBootWebFluxDemo.model.Person;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends ReactiveMongoRepository<Person,Integer> {
}
