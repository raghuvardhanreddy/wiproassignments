package com.example.SpringBootWebFluxDemo.service;

import com.example.SpringBootWebFluxDemo.model.Person;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PersonServiceInterface {
    void createPerson(Person person);
    Flux<Person> findAllPerson();
    Mono<Person> findByPersonId(Integer id);
}
