package assignments;
import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.concurrent.ExecutorService;

import java.util.concurrent.Executors;

class Tasks4 implements Runnable {

	private String name;

	public Tasks4(String name) {

		super();

		this.name = name;

	}

	@Override

	public void run() {

		try {

			for (int i = 1; i <= 5; i++) {

				Date dObj = new Date();

				SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");

				if (i == 1) {

					System.out.println("Task initialization : " +

	              "Task name : " + name +

	              " = " +

	              ft.format(dObj));

				} else {

					System.out.println("Executing Time for Task : " +

	              "Task name : " + name +

	              " = " +

	              ft.format(dObj));

				}

				Thread.sleep(1000);

			}

			System.out.println(name + " completed ");

		} catch (InterruptedException e) {

			e.printStackTrace();

		}

	}

}

public class Task5{

	static final int MAX_T = 3;

	public static void main(String[] args) {

		Runnable r1 = new Tasks4("scanning");

		Runnable r2 = new Tasks4("printing");

		Runnable r3 = new Tasks4("reading");

		Runnable r4 = new Tasks4("writing");

		Runnable r5 = new Tasks4("downloading");

		ExecutorService pool = Executors.newFixedThreadPool(MAX_T);

		pool.execute(r1);

		pool.execute(r2);

		pool.execute(r3);

		pool.execute(r4);

		pool.execute(r5);

		// Properly shut down the executor service

		pool.shutdown();

		try {

			if (!pool.awaitTermination(60, java.util.concurrent.TimeUnit.SECONDS)) {

				pool.shutdownNow();

			}

		} catch (InterruptedException e) {

			pool.shutdownNow();

		}

	}

}


