package com.example.Billing_service.controller;

import com.example.Billing_service.model.Billing;
import com.example.Billing_service.repository.BillingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BillingController {

    @Autowired
    BillingRepository billingRepo;

    @PostMapping("/bills")
    public ResponseEntity<Billing> createBilling(@RequestBody Billing billing) {
        try {
            Billing bill = billingRepo.save(new Billing(billing.getCustomer_id(), billing.getOrder_id()));
            return new ResponseEntity<>(bill, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/bills")
    public ResponseEntity<List<Billing>> getAllBills() {
        try {
            List<Billing> billing = new ArrayList<Billing>();
            billingRepo.findAll().forEach(billing::add);
            return new ResponseEntity<>(billing, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/bill/{id}")
    public ResponseEntity<Billing> getBillById(@PathVariable("id") long id){
        Optional<Billing> bill = billingRepo.findById(id);
        if(bill.isPresent()){
            return new ResponseEntity<>(bill.get(),HttpStatus.OK);
        }
        return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/bill/{id}")
    public ResponseEntity<Billing> updateBilling(@PathVariable("id") long id,@RequestBody Billing billing){
        Optional<Billing> billData = billingRepo.findById(id);
        if(billData.isPresent()){
            Billing bill = billData.get();
            bill.setCustomer_id(bill.getCustomer_id());
            bill.setOrder_id(bill.getOrder_id());

            return new ResponseEntity<>(billingRepo.save(bill),HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/bill/{id}")
    public ResponseEntity<HttpStatus> deleteBillById(@PathVariable("id") long id){
        try{
            billingRepo.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch(Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
