package com.example.Billing_service.model;

import jakarta.persistence.*;

@Entity
@Table(name="Billing")
public class Billing {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int bill_no;
    private int customer_id;
    private int order_id;

    public Billing(){

    }

    public Billing(int customer_id, int order_id) {
        this.customer_id = customer_id;
        this.order_id = order_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getBill_no() {
        return bill_no;
    }

    public void setBill_no(int bill_no) {
        this.bill_no = bill_no;
    }

    @Override
    public String toString() {
        return "Billing{" +
                "bill_no=" + bill_no +
                ", customer_id=" + customer_id +
                ", order_id=" + order_id +
                '}';
    }
}
