package com.example.Orders_service.controller;

import com.example.Orders_service.model.Orders;
import com.example.Orders_service.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrdersController {
    @Autowired
    OrdersRepository ordersRepo;

    @PostMapping("/orders")
    public ResponseEntity<Orders> createOrders(@RequestBody Orders orders){
        try{
            Orders odr=ordersRepo.save(new Orders(orders.getProduct_id(),orders.getOrder_number(),orders.getOrder_status()));
            return new ResponseEntity<>(odr, HttpStatus.CREATED);
        }catch (Exception e){
            return  new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders")
    public ResponseEntity<List<Orders>> getAllOrders(){
        try{
            List <Orders> orders=new ArrayList<Orders>();
            ordersRepo.findAll().forEach(orders::add);
            return  new ResponseEntity<>(orders, HttpStatus.OK);
        }catch (Exception e){
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/order/{id}")
    public ResponseEntity<Orders> getOrdersById(@PathVariable("id") long id){
        Optional<Orders> order = ordersRepo.findById(id);
        if(order.isPresent()){
            return new ResponseEntity<>(order.get(),HttpStatus.OK);
        }
        return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/order/{id}")
    public ResponseEntity<Orders> updateOrders(@PathVariable("id")long id,@RequestBody Orders orders){
        Optional<Orders> orderData=ordersRepo.findById(id);
        if(orderData.isPresent()){
            Orders odr=orderData.get();
            odr.setProduct_id(orders.getProduct_id());
            odr.setOrder_number(orders.getOrder_number());
            odr.setOrder_status(orders.getOrder_status());
            return  new ResponseEntity<>(ordersRepo.save(odr),HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }


    }

    @DeleteMapping("/order/{id}")
    public ResponseEntity<HttpStatus> deleteOrderById(@PathVariable("id") long id){
        try{
            ordersRepo.deleteById(id);
            return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
