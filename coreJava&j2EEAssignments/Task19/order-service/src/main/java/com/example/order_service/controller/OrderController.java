package com.example.order_service.controller;

import com.example.order_service.model.Order;
import com.example.order_service.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderController {

    @Autowired
    OrderRepository orderRepo;

    @PostMapping("/orders")
    public ResponseEntity<Order> createOrder(@RequestBody Order order){
        try{
       Order odr=orderRepo.save(new Order(order.getProduct_id(),order.getOrder_number(),order.getOrder_status()));
       return new ResponseEntity<>(odr, HttpStatus.CREATED);
        }catch (Exception e){
            return  new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrders(){
        try{
            List <Order> orders=new ArrayList<Order>();
            orderRepo.findAll().forEach(orders::add);
            return  new ResponseEntity<>(orders, HttpStatus.OK);
        }catch (Exception e){
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/order/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable("id") long id){
        Optional<Order> order = orderRepo.findById(id);
        if(order.isPresent()){
            return new ResponseEntity<>(order.get(),HttpStatus.OK);
        }
        return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/order/{id}")
    public ResponseEntity<Order> updateOrder(@PathVariable("id")long id,@RequestBody Order order){
        Optional<Order> orderData=orderRepo.findById(id);
        if(orderData.isPresent()){
            Order odr=orderData.get();
            odr.setProduct_id(order.getProduct_id());
            odr.setOrder_number(order.getOrder_number());
            odr.setOrder_status(order.getOrder_status());
            return  new ResponseEntity<>(orderRepo.save(odr),HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }


    }

    @DeleteMapping("/order/{id}")
    public ResponseEntity<HttpStatus> deleteOrderById(@PathVariable("id") long id){
        try{
            orderRepo.deleteById(id);
            return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
