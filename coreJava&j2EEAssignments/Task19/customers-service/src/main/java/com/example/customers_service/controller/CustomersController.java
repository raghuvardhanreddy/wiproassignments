package com.example.customers_service.controller;

import com.example.customers_service.model.Customers;
import com.example.customers_service.repository.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomersController {
    @Autowired
    CustomersRepository customersRepo;

    @PostMapping("/customers")

    public ResponseEntity<Customers> createCustomers(@RequestBody Customers customers) {
        try {
            Customers cust = customersRepo.save(new Customers(customers.getCustomer_name(), customers.getCustomer_email(),customers.getCustomer_phone()));
            return new ResponseEntity<>(cust, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers")
    public ResponseEntity<List<Customers>> getAllCustomers() {
        try {
            List<Customers> customers = new ArrayList<Customers>();
            customersRepo.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<Customers> getCustomersById(@PathVariable("id") long id){
        Optional<Customers> customers = customersRepo.findById(id);
        if(customers.isPresent()){
            return new ResponseEntity<>(customers.get(),HttpStatus.OK);
        }
        return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<Customers> updateCustomers(@PathVariable("id") long id,@RequestBody Customers customers){
        Optional<Customers> customersData = customersRepo.findById(id);
        if(customersData.isPresent()){
            Customers cust = customersData.get();
            cust.setCustomer_name(customers.getCustomer_name());
            cust.setCustomer_email(customers.getCustomer_email());
            cust.setCustomer_phone(customers.getCustomer_phone());
            return new ResponseEntity<>(customersRepo.save(cust),HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<HttpStatus> deleteCustomersById(@PathVariable("id") long id){
        try{
            customersRepo.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch(Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
