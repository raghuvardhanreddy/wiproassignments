package com.example.customers_service.repository;


import com.example.customers_service.model.Customers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomersRepository extends JpaRepository<Customers,Long> {
}
